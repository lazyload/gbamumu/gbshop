//
//  GBShopUITests.swift
//  GBShopUITests
//
//  Created by Евгений Кириллов on 24/10/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import XCTest

class GBShopUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
        
    }
    
    func testSuccess() {
        
        snapshot("Login screen")
        
        app.textFields["Имя пользователя"].tap()
        app.textFields["Имя пользователя"].typeText("admin")

        app.secureTextFields["Пароль"].tap()
        app.secureTextFields["Пароль"].typeText("123456")
        
        app.staticTexts["GeekBrains"].tap()     // скрытие клавиатуры
        app.buttons["Войти"].tap()
        
        let tabBarsQuery = app.tabBars
        
        tabBarsQuery.buttons["Товары"].tap()
        snapshot("Catalog screen")
        
        tabBarsQuery.buttons["Профиль"].tap()
        snapshot("Profile screen")
        
    }
    
}
