//
//  UserInfoRequest.swift
//  PerfectTemplate
//
//  Created by Евгений Кириллов on 06/11/2018.
//

import Foundation

/// Данные о пользователе, содержащиеся в запросе на регистрацию и изменение личных данных
struct UserInfoRequest: Decodable {
    
    let id_user: Int
    let username: String
    let password: String
    let email: String
    let gender: String
    let credit_card: String
    let bio: String
    
}
