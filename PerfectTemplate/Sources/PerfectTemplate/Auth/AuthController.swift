//
//  AuthController.swift
//  PerfectTemplate
//
//  Created by Евгений Кириллов on 06/11/2018.
//

import Foundation
import PerfectHTTP

/// Обрабатывает запросы в личный кабинет
class AuthController {
    
    /// Обработчик запроса на вход в личный кабинет
    let login: PerfectRequest = { request, response in
        let loginParams = request.params()
        
        let hasTwoParams = loginParams.count == 2
        let hasValidFirstParamName = loginParams[0].0 == "username"
        let hasValidSecondParamName = loginParams[1].0 == "password"
        
        let successfulResponse: ResponseBody = [
            "result": 1,
            "user": [
                "id_user": 123,
                "user_login": "geekbrains",
                "user_name": "John",
                "user_lastname": "Doe"
            ]
        ]
        
        if hasTwoParams, hasValidFirstParamName, hasValidSecondParamName {
            try! response.setBody(json: successfulResponse)
            response.completed()
        } else {
            try! response.setBody(json: ["result": 0, "errorMessage": "Неверные параметры запроса"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Incorrect request"))
        }
    }
    
    /// Обработчик запроса на выход из личного кабинета
    let logout: PerfectRequest = { request, response in
        let logoutParams = request.params()
        
        let hasOneParam = logoutParams.count == 1
        let hasValidParamName = logoutParams[0].0 == "id_user"
        
        if hasOneParam, hasValidParamName {
            try! response.setBody(json: ["result": 1])
            response.completed()
        } else {
            try! response.setBody(json: ["result": 0, "errorMessage": "Неверный параметр запроса"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Incorrect request"))
        }
    }
    
    /// Обработчик запроса на изменения данных пользователя
    let changeUserData: PerfectRequest = { request, response in
        do {
            let _ = try request.decode(UserInfoRequest.self)
            try response.setBody(json: ["result": 1])
            response.completed()
        } catch {
            try! response.setBody(json: ["result": 0, "errorMessage": "Не удалось декодировать запрос"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Decoding error - \(error)"))
        }
        
    }
    
    /// Обработчик запроса на регистрацию
    let registerUser: PerfectRequest = { request, response in
        do {
            let _ = try request.decode(UserInfoRequest.self)
            try response.setBody(json: ["result": 1, "userMessage": "Регистрация прошла успешно!"])
            response.completed()
        } catch {
            try! response.setBody(json: ["result": 0, "errorMessage": "Не удалось декодировать запрос"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Decoding error - \(error)"))
        }
    }

}
