//
//  BasketElementRequest.swift
//  PerfectTemplate
//
//  Created by Евгений Кириллов on 13/11/2018.
//

import Foundation

/// Элемент корзины, отправляющийся на сервер
struct BasketElementRequest: Decodable {
    
    let id_product: Int
    let quantity: Int
    
}
