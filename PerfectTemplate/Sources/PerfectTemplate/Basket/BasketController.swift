//
//  BasketController.swift
//  PerfectTemplate
//
//  Created by Евгений Кириллов on 13/11/2018.
//

import Foundation
import PerfectHTTP

class BasketController {
    
    /// Обработчик запроса на добавление товара в корзину
    let addElement: PerfectRequest = { request, response in
        do {
            let _ = try request.decode(BasketElementRequest.self)
            try response.setBody(json: ["result": 1])
            response.completed()
        } catch {
            try! response.setBody(json: ["result": 0, "errorMessage": "Не удалось декодировать запрос"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Decoding error - \(error)"))
        }
    }
    
    /// Обработчик запроса на удаление товара из корзины
    let deleteElement: PerfectRequest = { request, response in
        let requestBody = try! request.decode([String: Int].self)
        if requestBody.keys.first == "id_product" {
            try! response.setBody(json: ["result": 1])
            response.completed()
        } else {
            try! response.setBody(json: ["result": 0, "errorMessage": "Неверный формат запроса"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Unknown request"))
        }
    }
    
    /// Обработчик запроса на оплату содержимого корзины
    let pay: PerfectRequest = { request, response in
        do {
            try response.setBody(json: ["result": 1])
            response.completed()
        } catch {
            try! response.setBody(json: ["result": 0, "errorMessage": "Не удалось декодировать запрос"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Decoding error - \(error)"))
        }
    }
    
}
