//
//  main.swift
//  PerfectTemplate
//
//  Created by Kyle Jessup on 2015-11-05.
//	Copyright (C) 2015 PerfectlySoft, Inc.
//
//===----------------------------------------------------------------------===//
//
// This source file is part of the Perfect.org open source project
//
// Copyright (c) 2015 - 2016 PerfectlySoft Inc. and the Perfect project authors
// Licensed under Apache License v2.0
//
// See http://perfect.org/licensing.html for license information
//
//===----------------------------------------------------------------------===//
//

import PerfectHTTP
import PerfectHTTPServer

let server = HTTPServer()
var routes = Routes()

// MARK: - Работа с личным кабинетом

let authController = AuthController()
routes.add(method: .get, uri: "/login", handler: authController.login)
routes.add(method: .get, uri: "/logout", handler: authController.logout)
routes.add(method: .post, uri: "/changeUserData", handler: authController.changeUserData)
routes.add(method: .post, uri: "/registerUser", handler: authController.registerUser)

// MARK: - Работа с товарами

let prodController = ProdController()
routes.add(method: .get, uri: "/catalogData", handler: prodController.getCatalog)
routes.add(method: .get, uri: "/getGoodById", handler: prodController.getProduct)

// MARK: - Работа с отзывами

let revController = RevController()
routes.add(method: .get, uri: "/getReviews", handler: revController.getReviews)
routes.add(method: .post, uri: "/addReview", handler: revController.addReview)
routes.add(method: .delete, uri: "/removeReview", handler: revController.deleteReview)


// MARK: - Работа с корзиной

let basketController = BasketController()
routes.add(method: .post, uri: "/addToBasket", handler: basketController.addElement)
routes.add(method: .delete, uri: "/deleteFromBasket", handler: basketController.deleteElement)
routes.add(method: .post, uri: "/payBasket", handler: basketController.pay)


// MARK: - Запуск сервера

server.addRoutes(routes)
server.serverPort = 8080

do {
    try server.start()
} catch {
    fatalError("Network error - \(error)")
}
