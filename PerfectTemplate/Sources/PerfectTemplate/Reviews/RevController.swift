//
//  RevController.swift
//  PerfectTemplate
//
//  Created by Евгений Кириллов on 12/11/2018.
//

import Foundation
import PerfectHTTP

class RevController {

    /// Обработчик запроса на получение отзывов
    let getReviews: PerfectRequest = { request, response in
        let reviewsParams = request.params()
        
        let hasOneParam = reviewsParams.count == 1
        let hasValidParamName = reviewsParams[0].0 == "id_good"
        
        let successfulResponse = [
            [
                "review_id": 1,
                "review_text": "Мощный игровой ноутбук"
            ],
            [
                "review_id": 77,
                "review_text": "Так себе ноутбук"
            ]
        ]
        
        if hasOneParam, hasValidParamName {
            try! response.setBody(json: successfulResponse)
            response.completed()
        } else {
            try! response.setBody(json: ["result": 0, "errorMessage": "Неверные параметры запроса"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Incorrect request"))
        }
    }
    
    /// Обработчик запроса на добавление отзыва
    let addReview: PerfectRequest = { request, response in
        do {
            let _ = try request.decode(ReviewRequest.self)
            try response.setBody(json: ["result": 1])
            response.completed()
        } catch {
            try! response.setBody(json: ["result": 0, "errorMessage": "Не удалось декодировать запрос"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Decoding error - \(error)"))
        }
    }
    
    /// Обработчик запроса на удаление отзыва
    let deleteReview: PerfectRequest = { request, response in
        let requestBody = try! request.decode([String: Int].self)
        if requestBody.keys.first == "id_comment" {
            try! response.setBody(json: ["result": 1])
            response.completed()
        } else {
            try! response.setBody(json: ["result": 0, "errorMessage": "Неверный формат запроса"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Unknown request"))
        }
    }
    
}
