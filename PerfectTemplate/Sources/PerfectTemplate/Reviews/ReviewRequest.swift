//
//  ReviewRequest.swift
//  PerfectTemplate
//
//  Created by Евгений Кириллов on 12/11/2018.
//

import Foundation

/// Отзыв, отправляющийся на сервер
struct ReviewRequest: Decodable {
    
    let id_user: Int
    let text: String
    
}
