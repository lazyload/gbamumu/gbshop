//
//  Common.swift
//  PerfectTemplate
//
//  Created by Евгений Кириллов on 12/11/2018.
//

import PerfectHTTP

typealias PerfectRequest = (HTTPRequest, HTTPResponse) -> ()
typealias ResponseBody = [String: Any]
