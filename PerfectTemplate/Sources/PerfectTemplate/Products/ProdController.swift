//
//  ProdController.swift
//  PerfectTemplate
//
//  Created by Евгений Кириллов on 12/11/2018.
//

import PerfectHTTP

class ProdController {
    
    /// Обработчик запроса на получение каталога товаров
    let getCatalog: PerfectRequest = { request, response in
        let catalogParams = request.params()
        
        let hasTwoParams = catalogParams.count == 2
        let hasValidFirstParamName = catalogParams[0].0 == "page_number"
        let hasValidSecondParamName = catalogParams[1].0 == "id_category"
        
        let successfulResponse: ResponseBody = [
            "page_number": 1,
            "products": [
                [
                    "id_product": 123,
                    "product_name": "Ноутбук",
                    "price": 45600
                ],
                [
                    "id_product": 456,
                    "product_name": "Мышка",
                    "price": 1000
                ]
            ]
        ]
        
        if hasTwoParams, hasValidFirstParamName, hasValidSecondParamName {
            try! response.setBody(json: successfulResponse)
            response.completed()
        } else {
            try! response.setBody(json: ["result": 0, "errorMessage": "Неверные параметры запроса"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Incorrect request"))
        }
    }
    
    /// Обработчик запроса на получение товара
    let getProduct: PerfectRequest = { request, response in
        let productParams = request.params()
        
        let hasOneParam = productParams.count == 1
        let hasValidParamName = productParams[0].0 == "id_product"
        
        let successfulResponse: ResponseBody = [
            "result": 1,
            "product_name": "Название",
            "product_price": 123,
            "product_description": "Описание"
        ]
        
        if hasOneParam, hasValidParamName {
            try! response.setBody(json: successfulResponse)
            response.completed()
        } else {
            try! response.setBody(json: ["result": 0, "errorMessage": "Неверные параметры запроса"])
            response.completed(status: HTTPResponseStatus
                .custom(code: 500, message: "Incorrect request"))
        }
    }
    
}
