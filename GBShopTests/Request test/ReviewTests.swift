//
//  ReviewTests.swift
//  GBShopTests
//
//  Created by Евгений Кириллов on 08/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import OHHTTPStubs
import Alamofire
import XCTest
@testable import GBShop

/// Тестирование запросов на полученине отзывов о товарах
class ReviewTests: XCTestCase {
    
    // MARK: - Настройка

    var errorParser: AbstractErrorParser!
    var errorHandler: AbstractErrorHandler!
    var oStubsHelper: OStubHelper!
    var sessionManager: SessionManager!
    var revFactory: ReviewRequestFactory!
    let failureMessage = "Нужный ответ не получен"
    
    override func setUp() {
        
        errorParser = ErrorParserStub()
        errorHandler = ErrorHandlerStub()
        oStubsHelper = OStubHelper()
        
        let config = URLSessionConfiguration.ephemeral
        OHHTTPStubs.setEnabled(true, for: config)
        sessionManager = SessionManager(configuration: config)
        
        revFactory = Rev(errorParser: errorParser, errorHandler: errorHandler, sessionManager: sessionManager)
        
        super.setUp()
        
    }
    
    override func tearDown() {
        
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
        
    }
    
    // MARK: - Тесты
    
    func testGetReviewRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос отзывов")
        let responseStubURL = oStubsHelper.getStubFile(name: "ReviewStub")
        oStubsHelper.setGetStub(with: responseStubURL, pathEnd: "getReviews.json")
        
        revFactory.getReviews(for: 12) { [weak self] response in
            XCTAssertEqual(response.count, 2, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }
    
    func testAddReviewRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос отправки отзыва")
        let responseStubURL = oStubsHelper.getStubFile(name: "SimpleResultStub")
        oStubsHelper.setPostStub(with: responseStubURL, pathEnd: "addReview.json")
        
        revFactory.addReview("", by: 0) { [weak self] response in
            XCTAssertEqual(response.result, 1, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }
    
    func testRemoveReviewRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос удаления отзыва")
        let responseStubURL = oStubsHelper.getStubFile(name: "SimpleResultStub")
        oStubsHelper.setDeleteStub(with: responseStubURL, pathEnd: "removeReview.json")
        
        revFactory.removeReview(by: 0) { [weak self] response in
            XCTAssertEqual(response.result, 1, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }

}
