//
//  BasketTests.swift
//  GBShopTests
//
//  Created by Евгений Кириллов on 11/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import OHHTTPStubs
import Alamofire
import XCTest
@testable import GBShop

/// Тестирование запросов к корзине
class BasketTests: XCTestCase {
    
    // MARK: - Настройка

    var errorParser: AbstractErrorParser!
    var errorHandler: AbstractErrorHandler!
    var oStubsHelper: OStubHelper!
    var sessionManager: SessionManager!
    var basketFactory: BasketRequestFactory!
    let failureMessage = "Нужный ответ не получен"
    
    override func setUp() {
        
        errorParser = ErrorParserStub()
        errorHandler = ErrorHandlerStub()
        oStubsHelper = OStubHelper()
        
        let config = URLSessionConfiguration.ephemeral
        OHHTTPStubs.setEnabled(true, for: config)
        sessionManager = SessionManager(configuration: config)
        
        basketFactory = Basket(errorParser: errorParser, errorHandler: errorHandler, sessionManager: sessionManager)
        
        super.setUp()
        
    }
    
    override func tearDown() {
        
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
        
    }
    
    // MARK: - Тесты
    
    func testAddToRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос добавления товара в корзину")
        let responseStubURL = oStubsHelper.getStubFile(name: "SimpleResultStub")
        oStubsHelper.setPostStub(with: responseStubURL, pathEnd: "addToBasket.json")
        
        basketFactory.addToBasket(productID: 0, quantity: 0) { [weak self] response in
            XCTAssertEqual(response.result, 1, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }
    
    func testDeleteFromRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос удаления товара из корзины")
        let responseStubURL = oStubsHelper.getStubFile(name: "SimpleResultStub")
        oStubsHelper.setDeleteStub(with: responseStubURL, pathEnd: "deleteFromBasket.json")
        
        basketFactory.deleteFromBasket(productID: 0) { [weak self] response in
            XCTAssertEqual(response.result, 1, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }
    
    func testPayRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос оплаты товаров из корзины")
        let responseStubURL = oStubsHelper.getStubFile(name: "SimpleResultStub")
        oStubsHelper.setPostStub(with: responseStubURL, pathEnd: "payBasket.json")
        
        basketFactory.payBasket() { [weak self] response in
            XCTAssertEqual(response.result, 1, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }

}
