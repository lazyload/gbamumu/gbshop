//
//  AuthRequestTests.swift
//  GBShopTests
//
//  Created by Евгений on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import OHHTTPStubs
import Alamofire
import XCTest
@testable import GBShop

/// Тестирование запросов в личный кабинет
class AuthRequestTests: XCTestCase {
    
    // MARK: - Настройка
    
    var errorParser: AbstractErrorParser!
    var errorHandler: AbstractErrorHandler!
    var oStubsHelper: OStubHelper!
    var sessionManager: SessionManager!
    var authFactory: AuthRequestFactory!
    
    let failureMessage = "Нужный ответ не получен"
    let userProfileStub = UserProfile(id: 777,
                                      name: "testName",
                                      password: "testPassword",
                                      email: "test@email.com",
                                      gender: .male,
                                      creditCard: "testCard",
                                      bio: "testBio")
    
    override func setUp() {
        
        errorParser = ErrorParserStub()
        errorHandler = ErrorHandlerStub()
        oStubsHelper = OStubHelper()
        
        let config = URLSessionConfiguration.ephemeral
        OHHTTPStubs.setEnabled(true, for: config)
        sessionManager = SessionManager(configuration: config)
        
        authFactory = Auth(errorParser: errorParser, errorHandler: errorHandler, sessionManager: sessionManager)
        
        super.setUp()
        
    }
    
    override func tearDown() {
        
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
        
    }
    
    // MARK: - Тесты
    
    func testLoginRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос входа")
        let responseStubURL = oStubsHelper.getStubFile(name: "LoginStub")
        oStubsHelper.setGetStub(with: responseStubURL, pathEnd: "login.json")
        
        authFactory.login(userName: "", password: "") { [weak self] response in
            XCTAssertEqual(response.result, 1, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }
    
    func testLogoutRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос выхода")
        let responseStubURL = oStubsHelper.getStubFile(name: "SimpleResultStub")
        oStubsHelper.setGetStub(with: responseStubURL, pathEnd: "logout.json")
        
        authFactory.logout(userID: 0) { [weak self] response in
            XCTAssertEqual(response.result, 1, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }
    
    func testChangeDataRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос изменения личных данных")
        let responseStubURL = oStubsHelper.getStubFile(name: "SimpleResultStub")
        oStubsHelper.setPostStub(with: responseStubURL, pathEnd: "changeUserData.json")
        
        authFactory.changeData(of: userProfileStub) { [weak self] response in
            XCTAssertEqual(response.result, 1, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }
    
    func testRegisterRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос регистрации")
        let responseStubURL = oStubsHelper.getStubFile(name: "SimpleResultStub")
        oStubsHelper.setPostStub(with: responseStubURL, pathEnd: "registerUser.json")
        
        authFactory.register(user: userProfileStub) { [weak self] response in
            XCTAssertEqual(response.result, 1, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }
    
}
