//
//  ProductRequestTests.swift
//  GBShopTests
//
//  Created by Евгений Кириллов on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import OHHTTPStubs
import Alamofire
import XCTest
@testable import GBShop

/// Тестирование запросов на получение товаров
class ProdRequestTests: XCTestCase {
    
    // MARK: - Настройка
    
    var errorParser: AbstractErrorParser!
    var errorHandler: AbstractErrorHandler!
    var oStubsHelper: OStubHelper!
    var sessionManager: SessionManager!
    var prodFactory: ProductRequestFactory!
    let failureMessage = "Нужный ответ не получен"
    
    override func setUp() {
        
        errorParser = ErrorParserStub()
        errorHandler = ErrorHandlerStub()
        oStubsHelper = OStubHelper()
        
        let config = URLSessionConfiguration.ephemeral
        OHHTTPStubs.setEnabled(true, for: config)
        sessionManager = SessionManager(configuration: config)
        
        prodFactory = Prod(errorParser: errorParser, errorHandler: errorHandler, sessionManager: sessionManager)
        
        super.setUp()
        
    }
    
    override func tearDown() {
        
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
        
    }
    
    // MARK: - Тесты
    
    func testGetCatalogRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос каталога товаров")
        let responseStubURL = oStubsHelper.getStubFile(name: "CatalogStub")
        oStubsHelper.setGetStub(with: responseStubURL, pathEnd: "catalogData.json")
        
        prodFactory.getCatalog(page: 0, categoryID: 0) { [weak self] response in
            XCTAssertEqual(response.count, 2, (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }
    
    func testGetProductRequest() {
        
        let expectation = self.expectation(description: "Успешный запрос товара")
        let responseStubURL = oStubsHelper.getStubFile(name: "ProductStub")
        oStubsHelper.setGetStub(with: responseStubURL, pathEnd: "getGoodById.json")
        
        prodFactory.getProduct(by: 0) { [weak self] response in
            XCTAssertEqual(response.productName, "Ноутбук", (self?.failureMessage)!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7)
        
    }

}
