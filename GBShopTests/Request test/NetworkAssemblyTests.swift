//
//  NetworkAssemblyTests.swift
//  GBShopTests
//
//  Created by Евгений on 23/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Swinject
import XCTest
@testable import GBShop

class FactoryAssemblerTests: XCTestCase {

    var di: Resolver!
    
    override func setUp() {
        
        di = FactoryAssembler().makeResolver()
        super.setUp()
        
    }
    
    func testAuthAssembly() {
        
        let factory = di.resolve(AuthRequestFactory.self)
        XCTAssertNotNil(factory, "Не удалось создать фабрику")
        
    }
    
    func testProdAssembly() {
        
        let factory = di.resolve(ProductRequestFactory.self)
        XCTAssertNotNil(factory, "Не удалось создать фабрику")
        
    }
    
    func testRevAssembly() {
        
        let factory = di.resolve(ReviewRequestFactory.self)
        XCTAssertNotNil(factory, "Не удалось создать фабрику")
        
    }
    
    func testBasketAssembly() {
        
        let factory = di.resolve(BasketRequestFactory.self)
        XCTAssertNotNil(factory, "Не удалось создать фабрику")
        
    }

}
