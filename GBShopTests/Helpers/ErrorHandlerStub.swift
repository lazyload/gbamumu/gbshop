//
//  ErrorHandlerStub.swift
//  GBShopTests
//
//  Created by Евгений Кириллов on 22/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Foundation
@testable import GBShop

/// Заглушка-обработчик ошибок
class ErrorHandlerStub: AbstractErrorHandler {
    
    func handle(_ error: Error) {
        
        print("Handled")
        
    }
    
}
