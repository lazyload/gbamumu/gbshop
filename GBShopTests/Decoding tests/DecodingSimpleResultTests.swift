//
//  DecodingSimpleResultTests.swift
//  GBShopTests
//
//  Created by Евгений on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import XCTest
@testable import GBShop

/// Тестирование декодирования простого ответа сервера
class DecodingSimpleResultTests: XCTestCase {
    
    func testSimpleResultParsing() {
        
        let simpleResultFile = Bundle(for: AuthRequestTests.self)
            .url(forResource: "SimpleResultStub", withExtension: "json")!
        let simpleResultStub = try! Data(contentsOf: simpleResultFile)
        
        let simpleResultInstance = try? JSONDecoder().decode(SimpleResult.self, from: simpleResultStub)
        
        let validResult = (simpleResultInstance?.result == 1)
        let validMessage = (simpleResultInstance?.userMessage == "Ваш отзыв был передан на модерацию")
        
        XCTAssert(validResult && validMessage, "Не удалось декодировать JSON")
        
    }
    
}
