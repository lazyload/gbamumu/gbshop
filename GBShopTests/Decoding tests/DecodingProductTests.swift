//
//  DecodingProductTests.swift
//  GBShopTests
//
//  Created by Евгений Кириллов on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import XCTest
@testable import GBShop

/// Тестирование декодирования товара
class DecodingProductTests: XCTestCase {
    
    func testProductParsing() {
        
        let productFile = Bundle(for: AuthRequestTests.self)
            .url(forResource: "ProductStub", withExtension: "json")!
        let productStub = try! Data(contentsOf: productFile)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let productInstance = try? decoder.decode(Product.self, from: productStub)
        
        let validName = (productInstance?.productName == "Ноутбук")
        let validPrice = (productInstance?.productPrice == 45600)
        let validDescription = (productInstance?.productDescription == "Мощный игровой ноутбук")
        let validAllFields = validName && validPrice && validDescription
        
        XCTAssert(validAllFields, "Не удалось декодировать JSON")
        
    }
    
}
