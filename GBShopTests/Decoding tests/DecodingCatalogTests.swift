//
//  DecodingCatalogTests.swift
//  GBShopTests
//
//  Created by Евгений on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import XCTest
@testable import GBShop

/// Тестирование декодирования каталога
class DecodingCatalogTests: XCTestCase {
    
    func testCatalogProductParsing() {
        
        let catalogFile = Bundle(for: AuthRequestTests.self)
            .url(forResource: "CatalogStub", withExtension: "json")!
        let catalogStub = try! Data(contentsOf: catalogFile)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let catalogInstance = try? decoder.decode([CatalogProduct].self, from: catalogStub)
        
        let validCount = (catalogInstance?.count == 2)
        let validID = (catalogInstance?.first?.idProduct == 123)
        let validName = (catalogInstance?.first?.productName == "Ноутбук")
        let validPrice = (catalogInstance?.first?.price == 45600)
        let validAllFields = validCount && validID && validName && validPrice
        
        XCTAssert(validAllFields, "Не удалось декодировать JSON")
        
    }
    
}
