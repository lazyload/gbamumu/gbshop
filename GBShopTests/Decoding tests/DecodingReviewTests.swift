//
//  DecodingReviewTests.swift
//  GBShopTests
//
//  Created by Евгений Кириллов on 08/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import XCTest
@testable import GBShop

/// Тестирование декодирования отзыва
class DecodingReviewTests: XCTestCase {

    func testReviewParsing() {
        
        let reviewsFile = Bundle(for: AuthRequestTests.self)
            .url(forResource: "ReviewStub", withExtension: "json")!
        let reviewsStub = try! Data(contentsOf: reviewsFile)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let reviewsInstance = try? decoder.decode([Review].self, from: reviewsStub)
        
        let validCount = (reviewsInstance?.count == 2)
        let validID = (reviewsInstance?.first?.reviewId == 7)
        let validText = (reviewsInstance?.first?.reviewText == "Мощный игровой ноутбук")
        let validAllFields = validCount && validID && validText
        
        XCTAssert(validAllFields, "Не удалось декодировать JSON")
        
    }

}
