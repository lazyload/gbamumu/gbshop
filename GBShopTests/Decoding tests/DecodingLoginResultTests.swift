//
//  DecodingLoginResultTests.swift
//  GBShopTests
//
//  Created by Евгений on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import XCTest
@testable import GBShop

/// Тестирование декодирования ответа на вход пользователя
class DecodingLoginResultTests: XCTestCase {
    
    func testLoginResultParsing() {
        
        let loginResultFile = Bundle(for: AuthRequestTests.self)
            .url(forResource: "LoginStub", withExtension: "json")!
        let loginResultStub = try! Data(contentsOf: loginResultFile)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let loginResultInstance = try? decoder.decode(LoginResult.self, from: loginResultStub)
        
        let validResult = (loginResultInstance?.result == 1)
        let validUserID = (loginResultInstance?.user.idUser == 123)
        let validLogin = (loginResultInstance?.user.userLogin == "geekbrains")
        let validName = (loginResultInstance?.user.userName == "John")
        let validLastname = (loginResultInstance?.user.userLastname == "Doe")
        let validAllFields = validResult && validUserID && validLogin && validName && validLastname
        
        XCTAssert(validAllFields, "Не удалось декодировать JSON")
        
    }
    
}
