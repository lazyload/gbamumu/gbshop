//
//  UserKeeperTests.swift
//  GBShopTests
//
//  Created by Евгений Кириллов on 22/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import XCTest
@testable import GBShop

class DefaultsUserKeeperTests: XCTestCase {

    var storageMok: UserDefaults!
    var userKeeper: UserKeeper!
    let user = User(idUser: 777,
                    userLogin: "login",
                    userName: "name",
                    userLastname: "lastname")
    
    override func setUp() {
        
        storageMok = UserDefaults.init(suiteName: "test storage")
        userKeeper = DefaultsUserKeeper(with: storageMok)
        super.setUp()
        
    }
    
    override func tearDown() {
        
        storageMok.removeObject(forKey: "id")
        storageMok.removeObject(forKey: "login")
        storageMok.removeObject(forKey: "firstName")
        storageMok.removeObject(forKey: "lastName")
        super.tearDown()
        
    }
    
    func testSave() {
        
        userKeeper.save(user: user)
        let loadedUser = userKeeper.load()
        
        let validID = user.idUser == loadedUser?.idUser
        let validLogin = user.userLogin == loadedUser?.userLogin
        let validName = user.userName == loadedUser?.userName
        let validLastname = user.userLastname == loadedUser?.userLastname
        let allValid = validID && validLogin && validName && validLastname
        
        XCTAssert(allValid, "Данные сохранены или загружены неверно")
        
    }
    
    func testLoadNil() {
        
        let loadedUser = userKeeper.load()
        XCTAssertNil(loadedUser, "Загружены чужие данные")
        
    }

}
