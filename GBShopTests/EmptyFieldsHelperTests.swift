//
//  EmptyFieldsHelperTests.swift
//  GBShopTests
//
//  Created by Евгений Кириллов on 18/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import XCTest
import UIKit
@testable import GBShop

class EmptyFieldsHelperTests: XCTestCase {

    var helper: EmptyFieldsHighlighter!
    var firstField: UITextField!
    var secondField: UITextField!
    var fields = [UITextField]()
    
    override func setUp() {
        
        helper = EmptyFieldsHighlighter()
        firstField = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        secondField = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        fields = [firstField, secondField]
        
        super.setUp()
        
    }
    
    func testNil() {
        
        firstField.text = nil
        secondField.text = nil
        
        let didHighlight = helper.didHighlightEmptyFields(fields) { _ in }
        XCTAssert(didHighlight, "Поля не были выделены")
        
    }
    
    func testEmpty() {
        
        firstField.text = ""
        secondField.text = ""
        
        let didHighlight = helper.didHighlightEmptyFields(fields) { _ in }
        XCTAssert(didHighlight, "Поля не были выделены")
        
    }
    
    func testFilled() {
        
        firstField.text = "First text"
        secondField.text = "Second text"
        
        let didHighlight = helper.didHighlightEmptyFields(fields) { _ in }
        XCTAssertFalse(didHighlight, "Поля были выделены")
        
    }
    
    func testFilledEmpty() {
        
        firstField.text = "First text"
        secondField.text = ""
        
        let didHighlight = helper.didHighlightEmptyFields(fields) { _ in }
        XCTAssert(didHighlight, "Поля не были выделены")
        
    }
    
    func testFilledNil() {
        
        firstField.text = "First text"
        secondField.text = nil
        
        let didHighlight = helper.didHighlightEmptyFields(fields) { _ in }
        XCTAssert(didHighlight, "Поля не были выделены")
        
    }
    
    func testEmptyNil() {
        
        firstField.text = ""
        secondField.text = nil
        
        let didHighlight = helper.didHighlightEmptyFields(fields) { _ in }
        XCTAssert(didHighlight, "Поля не были выделены")
        
    }
    
}
