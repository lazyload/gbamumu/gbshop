//
//  AppDelegate.swift
//  GBShop
//
//  Created by Евгений Кириллов on 24/10/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Answers.self, Crashlytics.self])
        
        return true
        
    }
    
}
