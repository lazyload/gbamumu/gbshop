//
//  HelperAssembly.swift
//  GBShop
//
//  Created by Евгений Кириллов on 10/12/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Swinject

/// Сборщик хранителя пользователя
final class HelperAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(UserKeeper.self) { _ in
            let storage = UserDefaults.standard
            return DefaultsUserKeeper(with: storage)
        }
        
        container.register(EmptyFieldsChecker.self) { _ in
            EmptyFieldsHighlighter()
        }
        
    }
    
}
