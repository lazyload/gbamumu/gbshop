//
//  FactoryAssembler.swift
//  GBShop
//
//  Created by Евгений on 23/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Swinject

let diAssembler = FactoryAssembler().makeResolver()

/// Сборщик DI контейнеров
final class FactoryAssembler {
    
    func makeResolver() -> Resolver {
        
        let assembly = Assembler([NetworkAssembly(),
                                  HelperAssembly()])
        return assembly.resolver
        
    }
    
}
