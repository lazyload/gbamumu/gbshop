//
//  NetworkAssembly.swift
//  GBShop
//
//  Created by Евгений Кириллов on 23/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire
import Swinject

/// Сборщик фабрик для работы с сетью
final class NetworkAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(AbstractErrorParser.self) { _ in
            AppErrorParser()
        }.inObjectScope(.container)
        
        container.register(AbstractErrorHandler.self) { _ in
            AlertErrorHandler()
        }.inObjectScope(.container)
        
        container.register(SessionManager.self) { _ in
            let configuration = URLSessionConfiguration.default
            configuration.httpShouldSetCookies = false
            configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
            let manager = SessionManager(configuration: configuration)
            return manager
        }
        
        container.register(AuthRequestFactory.self) { resolver in
            Auth(errorParser: resolver.resolve(AbstractErrorParser.self)!,
                 errorHandler: resolver.resolve(AbstractErrorHandler.self)!,
                 sessionManager: resolver.resolve(SessionManager.self)!,
                 queue: DispatchQueue.global())
        }
        
        container.register(ProductRequestFactory.self) { resolver in
            Prod(errorParser: resolver.resolve(AbstractErrorParser.self)!,
                 errorHandler: resolver.resolve(AbstractErrorHandler.self)!,
                 sessionManager: resolver.resolve(SessionManager.self)!,
                 queue: DispatchQueue.global())
        }
        
        container.register(ReviewRequestFactory.self) { resolver in
            Rev(errorParser: resolver.resolve(AbstractErrorParser.self)!,
                errorHandler: resolver.resolve(AbstractErrorHandler.self)!,
                sessionManager: resolver.resolve(SessionManager.self)!,
                queue: DispatchQueue.global())
        }
        
        container.register(BasketRequestFactory.self) { resolver in
            Basket(errorParser: resolver.resolve(AbstractErrorParser.self)!,
                   errorHandler: resolver.resolve(AbstractErrorHandler.self)!,
                   sessionManager: resolver.resolve(SessionManager.self)!,
                   queue: DispatchQueue.global())
        }
        
    }
    
}
