//
//  Basket.swift
//  GBShop
//
//  Created by Евгений Кириллов on 11/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

/// Обработка запросов, связанных с корзиной
final class Basket: BaseRequestFactory, BasketRequestFactory {
    
    func addToBasket(productID: Int, quantity: Int, completion: @escaping (SimpleResult) -> Void) {
        
        let requestRouter = AddToRequest(baseURL: baseURL, productID: productID, quantity: quantity)
        self.request(request: requestRouter, completion: completion)
        
    }
    
    func deleteFromBasket(productID: Int, completion: @escaping (SimpleResult) -> Void) {
        
        let requestRouter = DeleteFromRequest(baseURL: baseURL, productID: productID)
        self.request(request: requestRouter, completion: completion)
        
    }
    
    func payBasket(completion: @escaping (SimpleResult) -> Void) {
        
        let requestRouter = PayRequest(baseURL: baseURL)
        self.request(request: requestRouter, completion: completion)
        
    }
    
}

// MARK: - Реализация вспомогательных моделей

extension Basket {
    
    /// Модель для добавления товара в корзину
    private struct AddToRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .post
        let path: String = "addToBasket.json"
        
        let productID: Int
        let quantity: Int
        var parameters: Parameters? {
            return ["id_product": productID,
                    "quantity": quantity]
        }
        
    }
    
    /// Модель для удаления товара из корзины
    private struct DeleteFromRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .delete
        let path: String = "deleteFromBasket.json"
        
        let productID: Int
        var parameters: Parameters? {
            return ["id_product": productID]
        }
        
    }
    
    /// Модель для оплаты товаров из корзины
    private struct PayRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .post
        let path: String = "payBasket.json"
        
        var parameters: Parameters? {
            return [:]
        }
        
    }
    
}
