//
//  BasketRequestFactory.swift
//  GBShop
//
//  Created by Евгений Кириллов on 11/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

/// Помогает реализовать запросы для работы с корзиной
protocol BasketRequestFactory {
    
    /// Добавить товар в корзину
    ///
    /// - Parameters:
    ///   - productID: Идентификатор товара
    ///   - quantity: Количество товара
    ///   - completion: Завершающий блок
    func addToBasket(productID: Int, quantity: Int, completion: @escaping (SimpleResult) -> Void)
    
    /// Удалить товар из корзины
    ///
    /// - Parameters:
    ///   - productID: Идентификатор товара
    ///   - completion: Завершающий блок
    func deleteFromBasket(productID: Int, completion: @escaping (SimpleResult) -> Void)
    
    /// Оплатить содержимое корзины
    ///
    /// - Parameter completion: Завершающий блок
    func payBasket(completion: @escaping (SimpleResult) -> Void)
    
}
