//
//  Prod.swift
//  GBShop
//
//  Created by Евгений Кириллов on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

/// Обработка запросов, связанных с товарами
final class Prod: BaseRequestFactory, ProductRequestFactory {
    
    func getCatalog(page: Int, categoryID: Int, completion: @escaping ([CatalogProduct]) -> Void) {
        
        let requestRouter = CatalogRequest(baseURL: baseURL, page: 1, category: 1)
        self.request(request: requestRouter, completion: completion)
        
    }
    
    func getProduct(by id: Int, completion: @escaping (Product) -> Void) {
        
        let requestRouter = GoodRequest(baseURL: baseURL, product: 123)
        self.request(request: requestRouter, completion: completion)
        
    }
    
}

// MARK: - Реализация вспомогательных моделей

extension Prod {
    
    /// Модель для получения каталога товаров
    private struct CatalogRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .get
        let path: String = "catalogData.json"
        
        let page: Int
        let category: Int
        var parameters: Parameters? {
            return ["page_number": page,
                    "id_category": category]
        }
        
    }
    
    /// Модель для получения товара
    private struct GoodRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .get
        let path: String = "getGoodById.json"
        
        let product: Int
        var parameters: Parameters? {
            return ["id_product": product]
        }
        
    }
    
}
