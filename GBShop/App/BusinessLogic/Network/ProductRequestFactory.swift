//
//  ProductRequestFactory.swift
//  GBShop
//
//  Created by Евгений Кириллов on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

/// Помогает реализовать запросы для работы с товарами
protocol ProductRequestFactory {
    
    /// Получение каталога товаров
    ///
    /// - Parameters:
    ///   - page: Номер страницы каталога
    ///   - categoryID: Идентификатор категории товара
    ///   - completion: Завершающий блок
    func getCatalog(page: Int, categoryID: Int, completion: @escaping ([CatalogProduct]) -> Void)
    
    /// Получение товара по идентификатору
    ///
    /// - Parameters:
    ///   - id: Идентификатор товара
    ///   - completion: Завершающий блок
    func getProduct(by id: Int, completion: @escaping (Product) -> Void)
    
}
