//
//  Auth.swift
//  GBShop
//
//  Created by Евгений Кириллов on 29/10/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

/// Обработка запросов в личный кабинет
final class Auth: BaseRequestFactory, AuthRequestFactory {
    
    func login(userName: String, password: String, completion: @escaping (LoginResult) -> Void) {
        
        let requestRouter = LoginRequest(baseURL: baseURL, login: userName, password: password)
        self.request(request: requestRouter, completion: completion)
        
    }
    
    func logout(userID: Int, completion: @escaping (SimpleResult) -> Void) {
        
        let requestRouter = LogoutRequest(baseURL: baseURL, userID: userID)
        self.request(request: requestRouter, completion: completion)
        
    }
    
    func changeData(of user: UserProfile, completion: @escaping (SimpleResult) -> Void) {
        
        let requestRouter = UserInfoRequest(baseURL: baseURL, apiMethod: .change, userProfile: user)
        self.request(request: requestRouter, completion: completion)
        
    }
    
    func register(user: UserProfile, completion: @escaping (SimpleResult) -> Void) {
        
        let requestRouter = UserInfoRequest(baseURL: baseURL, apiMethod: .register, userProfile: user)
        self.request(request: requestRouter, completion: completion)
        
    }
    
}

// MARK: - Реализация вспомогательных моделей

extension Auth {
   
    /// Модель запроса на вход в личный кабинет
    private struct LoginRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .get
        let path: String = "login.json"
        
        let login: String
        let password: String
        var parameters: Parameters? {
            return ["username": login,
                    "password": password]
        }
        
    }
    
    /// Модель запроса на выход из личного кабинета
    private struct LogoutRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .get
        let path: String = "logout.json"
        
        let userID: Int
        var parameters: Parameters? {
            return ["id_user": userID]
        }
        
    }
    
    /// Модель запроса на ввод или изменение сведений о пользователе
    private struct UserInfoRequest: RequestRouter {
        
        var baseURL: URL
        var method: HTTPMethod = .post
        var path: String
        
        let userProfile: UserProfile
        var parameters: Parameters? {
            return ["id_user": userProfile.id,
                    "username": userProfile.name,
                    "password": userProfile.password,
                    "email": userProfile.email,
                    "gender": userProfile.gender.rawValue,
                    "credit_card": userProfile.creditCard,
                    "bio": userProfile.bio
            ]
        }
        
        init(baseURL: URL, apiMethod: UserInfoMethod, userProfile: UserProfile) {
           
            self.baseURL = baseURL
            path = apiMethod.rawValue    // конструктор создан ради этой строчки
            self.userProfile = userProfile
        
        }
        
    }
    
    /// Варианты путей к API магазина
    ///
    /// - change: Изменить данные о пользователе
    /// - register: Зарегистрировать нового пользователя
    private enum UserInfoMethod: String {
        
        case change = "changeUserData.json"
        case register = "registerUser.json"
        
    }
    
}
