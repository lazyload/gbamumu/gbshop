//
//  AuthRequestFactory.swift
//  GBShop
//
//  Created by Евгений Кириллов on 29/10/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

/// Помогает реализовать запросы для работы с личным кабинетом
protocol AuthRequestFactory {
    
    /// Вход в личный кабинет
    ///
    /// - Parameters:
    ///   - userName: Имя пользователя магазина
    ///   - password: Пароль для входа
    ///   - completionHandler: Завершающий блок
    func login(userName: String, password: String, completion: @escaping (LoginResult) -> Void)

    /// Выход из личного кабинета
    ///
    /// - Parameters:
    ///   - userID: Идентификатор пользователя
    ///   - completionHandler: Завершающий блок
    func logout(userID: Int, completion: @escaping (SimpleResult) -> Void)
    
    /// Изменение данные о пользователе
    ///
    /// - Parameters:
    ///   - user: Профиль пользователя
    ///   - completionHandler: Завершающий блок
    func changeData(of user: UserProfile, completion: @escaping (SimpleResult) -> Void)
    
    /// Регистрация нового пользователя
    ///
    /// - Parameters:
    ///   - user: Профиль пользователя
    ///   - completionHandler: Завершающий блок
    func register(user: UserProfile, completion: @escaping (SimpleResult) -> Void)
    
}

/// Возможные варианты половой принадлежности
///
/// - male: Мужской пол
/// - female: Женский пол
enum Gender: String {
    
    case male = "m"
    case female = "f"
    
}
