//
//  ReviewRequestFactory.swift
//  GBShop
//
//  Created by Евгений Кириллов on 08/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

/// Помогает реализовать запросы для работы с отзывами
protocol ReviewRequestFactory {
    
    /// Получает массив отзывов о товаре
    ///
    /// - Parameters:
    ///   - goodID: Идентификатор товара
    ///   - completion: завершающий блок
    func getReviews(for goodID: Int, completion: @escaping ([Review]) -> Void)
    
    /// Добавляет отзыв от имени кокретного пользователя
    ///
    /// - Parameters:
    ///   - text: Текст отзыва
    ///   - userID: Идентификатор автора отзыва
    ///   - completion: завершающий блок
    func addReview(_ text: String, by userID: Int, completion: @escaping (SimpleResult) -> Void)
    
    /// Удаляет отзыв
    ///
    /// - Parameters:
    ///   - id: Идентификатор отзыва
    ///   - completion: завершающий блок
    func removeReview(by id: Int, completion: @escaping (SimpleResult) -> Void)
    
}
