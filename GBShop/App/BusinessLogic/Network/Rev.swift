//
//  Rev.swift
//  GBShop
//
//  Created by Евгений Кириллов on 08/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

/// Обработка запросов, связанных с отзывами
final class Rev: BaseRequestFactory, ReviewRequestFactory {
    
    func getReviews(for goodID: Int, completion: @escaping ([Review]) -> Void) {
        
        let requestRouter = GetReviewsRequest(baseURL: baseURL, goodID: goodID)
        self.request(request: requestRouter, completion: completion)
        
    }
    
    func addReview(_ text: String, by userID: Int, completion: @escaping (SimpleResult) -> Void) {
        
        let requestRouter = AddReviewRequest(baseURL: baseURL, authorID: 1, text: "Отзыв пользователя")
        self.request(request: requestRouter, completion: completion)
        
    }
    
    func removeReview(by id: Int, completion: @escaping (SimpleResult) -> Void) {
        
        let requestRouter = RemoveReviewRequest(baseURL: baseURL, reviewID: 1)
        self.request(request: requestRouter, completion: completion)
        
    }
    
}

// MARK: - Реализация вспомогательных моделей

extension Rev {
    
    /// Модель для получения отзывов
    private struct GetReviewsRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .get
        let path: String = "getReviews.json"
        
        let goodID: Int
        var parameters: Parameters? {
            return ["id_good": goodID]
        }
        
    }
    
    /// Модель для добавления отзыва
    private struct AddReviewRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .post
        let path: String = "addReview.json"
        
        let authorID: Int
        let text: String
        var parameters: Parameters? {
            return ["id_user": authorID,
                    "text": text]
        }
        
    }
    
    /// Модель для удаления отзыва
    private struct RemoveReviewRequest: RequestRouter {
        
        let baseURL: URL
        let method: HTTPMethod = .delete
        let path: String = "removeReview.json"
        
        let reviewID: Int
        var parameters: Parameters? {
            return ["id_comment": reviewID]
        }
        
    }
    
}
