//
//  PrintErrorHandler.swift
//  GBShop
//
//  Created by Евгений Кириллов on 22/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

/// Обработчик, распечатывающий ошибки
final class PrintErrorHandler: AbstractErrorHandler {
    
    func handle(_ error: Error) {
        
        print(error)
        
    }
    
}
