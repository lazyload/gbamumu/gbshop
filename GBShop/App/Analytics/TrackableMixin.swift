//
//  TrackableMixin.swift
//  GBShop
//
//  Created by Евгений on 30/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Fabric
import Crashlytics

/// Определяет методы взаимодействия с аналитической библиотекой
protocol TrackableMixin {
    
    /// Отследить событие с помощью аналитической библиотеки
    ///
    /// - Parameter event: Событие для отправки поставщику аналитики
    func track(_ event: AnalyticEvents)
    
}

extension TrackableMixin {
    
    func track(_ event: AnalyticEvents) {
        
        switch event {
            
        case let .login(user, success):
            Answers.logLogin(withMethod: "Введено имя: \(user)", success: success as NSNumber)
        
        case let .logout(user, success):
            let attributes: [String: Any] = ["Имя": user, "Успех": success]
            Answers.logCustomEvent(withName: "Выход из учётной записи", customAttributes: attributes)
        
        case let .registration(user, success):
            Answers.logSignUp(withMethod: "Стандартная регистрация",
                              success: success as NSNumber,
                              customAttributes: ["Имя пользователя": user])
        
        case .productListOpened:
            Answers.logCustomEvent(withName: "Просматривается спиок товаров")
        
        case .specificProductOpened:
            Answers.logCustomEvent(withName: "Просматривается товар")
        
        case .addedToCart:
            Answers.logCustomEvent(withName: "Товар добавлен в корзину")
        
        case .removedFromCart:
            Answers.logCustomEvent(withName: "Товар удалён из корзины")
        
        case .orderPaied:
            Answers.logCustomEvent(withName: "Заказ оплачен")
        
        case .reviewAdded:
            Answers.logCustomEvent(withName: "Опубликован отзыв")
        
        case .assertionFailure(let message):
            Crashlytics.sharedInstance().recordCustomExceptionName("Фатальная ошибка",
                                                                   reason: message,
                                                                   frameArray: [])
        }
        
    }
    
}
