//
//  AnalyticEvents.swift
//  GBShop
//
//  Created by Евгений on 30/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

/// События для отправки аналитики
enum AnalyticEvents {
    
    /// Вход в учётную запись
    case login(username: String, success: Bool)
    
    /// Выход из учётной записи
    case logout(username: String, success: Bool)
    
    /// Регистрация нового пользователя
    case registration(username: String, success: Bool)
    
    /// Открыт список товаров
    case productListOpened
    
    /// Открыт конкретный товар
    case specificProductOpened
    
    /// Товар добавлен в корзину
    case addedToCart
    
    /// Товар удалён из корзины
    case removedFromCart
    
    /// Заказ оплачен
    case orderPaied
    
    /// Добавлен отзыв о товаре
    case reviewAdded
    
    /// Ошибочное утверждение
    case assertionFailure(message: String)
    
}
