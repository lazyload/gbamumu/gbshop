//
//  AssertionFailureMixin.swift
//  GBShop
//
//  Created by Евгений on 30/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

/// Связывание ошибочного утверждения с аналитикой
protocol AssertionFailureMixin: TrackableMixin {
    
    /// Обрабатывает ошибочные утверждения
    ///
    /// - Parameter description: Описание ошибки
    func handleAssertionFailure(_ description: String)
    
}

extension AssertionFailureMixin {
    
    func handleAssertionFailure(_ description: String) {
        
        assertionFailure(description)
        track(AnalyticEvents.assertionFailure(message: description))
        
    }
    
}
