//
//  CatalogView.swift
//  GBShop
//
//  Created by Евгений Кириллов on 19/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

/// Табличное представление каталога товаров
final class CatalogView: UIView, ProductList, AssertionFailureMixin {
    
    @IBOutlet weak var list: UITableView!
    weak var listDataSource: CatalogDataSource?
    
    func update() {
        
        DispatchQueue.main.async {
            self.list.reloadData()
        }
        
    }

}

extension CatalogView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listDataSource?.catalog.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Catalog", for: indexPath)
        guard let product = listDataSource?.catalog[indexPath.row] else {
            handleAssertionFailure("Не удалось получить товар из каталога")
            return cell
            
        }
        
        cell.textLabel?.text = product.productName
        cell.detailTextLabel?.text = String(product.price) + " у.е."
        
        return cell
        
    }
    
}
