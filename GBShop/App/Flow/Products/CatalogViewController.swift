//
//  CatalogViewController.swift
//  GBShop
//
//  Created by Евгений Кириллов on 18/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

final class CatalogViewController: UIViewController, CatalogDataSource, TrackableMixin {
    
    private let factory = diAssembler.resolve(ProductRequestFactory.self)
    var catalog = [CatalogProduct]()
    private var customView: ProductList? {
        return viewIfLoaded as? ProductList
    }
    
    override func viewDidLoad() {
        
        navigationController?.navigationBar.prefersLargeTitles = true
        customView?.listDataSource = self
        
        loadCatalog()
        track(AnalyticEvents.productListOpened)
        
        super.viewDidLoad()
        
    }
    
    /// Загружает каталог товаров
    private func loadCatalog() {
        
        factory?.getCatalog(page: 1, categoryID: 1) { [weak self] catalog in
            self?.catalog = catalog
            self?.customView?.update()
        }
        
    }

}
