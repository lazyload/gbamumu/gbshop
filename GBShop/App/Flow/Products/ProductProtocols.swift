//
//  ProductProtocols.swift
//  GBShop
//
//  Created by Евгений Кириллов on 11/12/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Foundation

/// Источник данных в виде каталога товаров
protocol CatalogDataSource: class {
    
    /// Каталог товаров для отображения в таблице или коллекции
    var catalog: [CatalogProduct] { get set }
    
}

/// Определяет правила для работы со списком товаров
protocol ProductList: class {
    
    /// Источник данных для таблицы или коллекции
    var listDataSource: CatalogDataSource? { get set }
    
    /// Обновление таблицы или коллекции
    func update()
    
}
