//
//  ProductsViewController.swift
//  GBShop
//
//  Created by Евгений Кириллов on 23/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController, TrackableMixin {

    override func viewDidLoad() {
        
        track(AnalyticEvents.specificProductOpened)
        super.viewDidLoad()
        
    }
    
}
