//
//  UserKeeper.swift
//  GBShop
//
//  Created by Евгений Кириллов on 20/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Foundation
import Fabric
import Crashlytics

/// Определяет тип хранилища информации – хранилище `UserDefaults`
protocol DefaultsStorage {
    
    var storage: UserDefaults { get }
    
}

/// Определяет правила работы с хранителем вошедшего пользователя
protocol UserKeeper: DefaultsStorage {
    
    /// Сохранить переданного пользователя
    ///
    /// - Parameter user: Пользователь, данные о котором необходимо сохранить
    func save(user: User)
    
    /// Загрузить сохранённого пользователя
    ///
    /// - Returns: Сохранённый пользователь
    func load() -> User?
    
}

/// Хранит пользователя в `UserDefaults`
final class DefaultsUserKeeper: UserKeeper {
    
    var storage: UserDefaults
    
    func save(user: User) {
        
        storage.set(user.idUser, forKey: DefaultKeeperKeys.id.rawValue)
        storage.set(user.userLogin, forKey: DefaultKeeperKeys.login.rawValue)
        storage.set(user.userName, forKey: DefaultKeeperKeys.firstName.rawValue)
        storage.set(user.userLastname, forKey: DefaultKeeperKeys.lastName.rawValue)
        Crashlytics.sharedInstance().setUserName(user.userLogin)
        
    }
    
    func load() -> User? {
        
        guard
            let id = storage.value(forKey: DefaultKeeperKeys.id.rawValue) as? Int,
            let login = storage.value(forKey: DefaultKeeperKeys.login.rawValue) as? String,
            let name = storage.value(forKey: DefaultKeeperKeys.firstName.rawValue) as? String,
            let lastName = storage.value(forKey: DefaultKeeperKeys.lastName.rawValue) as? String else {
                return nil
        }
        
        return User(idUser: id,
                    userLogin: login,
                    userName: name,
                    userLastname: lastName)
        
    }
    
    init(with storage: UserDefaults = UserDefaults.standard) {
        
        self.storage = storage
        
    }
    
}

private enum DefaultKeeperKeys: String {
    
    case id
    case login
    case firstName
    case lastName
    
}
