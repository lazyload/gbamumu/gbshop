//
//  ProfileTable.swift
//  GBShop
//
//  Created by Евгений Кириллов on 18/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

/// Таблица, отображающая инормацию о пользователе
class ProfileTable: UITableView, ProfileView {

    // MARK: - Outlets
    
    @IBOutlet private weak var nameContainer: SwitchableTextView!
    @IBOutlet private weak var passwordContainer: SwitchableTextView!
    @IBOutlet private weak var emailContainer: SwitchableTextView!
    @IBOutlet private weak var genderContainer: SwitchableGender!
    @IBOutlet private weak var cardContainer: SwitchableTextView!
    @IBOutlet private weak var bioContainer: SwitchableTextView!
    
    // MARK: - Свойства
    
    private var containers = [SwitchableView]()
    
    weak var profileSource: ProfileDataSource? {
        willSet {
            let firstName = newValue?.source?.userName ?? ""
            let lastName = newValue?.source?.userLastname ?? ""
            nameContainer.text = firstName + " " + lastName
        }
    }
    
    var inEditingMode = false {
        didSet {
            containers.forEach { $0.isSwitchedOn = inEditingMode }
        }
    }
    
    // MARK: - Методы
    
    override func layoutSubviews() {
        
        containers = [nameContainer,
                      passwordContainer,
                      emailContainer,
                      genderContainer,
                      cardContainer,
                      bioContainer]
        super.layoutSubviews()
        
    }

}
