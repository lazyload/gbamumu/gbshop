//
//  ProfileProtocols.swift
//  GBShop
//
//  Created by Евгений Кириллов on 18/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

/// Требования к классу, являющемуся источником данных для отображения профиля пользователя
protocol ProfileDataSource: class {
    
    var source: User? { get }
    
}

/// Устанавливает ограничение на класс, отображающий профиль пользователя
protocol ProfileView: class {
    
    /// Режим отображения пользователя
    var inEditingMode: Bool { get set }
    
    /// Источник данных для отображения
    var profileSource: ProfileDataSource? { get set }
    
}
