//
//  ProfileViewController.swift
//  GBShop
//
//  Created by Евгений Кириллов on 18/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

final class ProfileViewController: UITableViewController, ProfileDataSource, AssertionFailureMixin {

    @IBOutlet weak var editingSwitcher: UIBarButtonItem!
    private var editingRecords = true
    var source: User?
    private var profileView: ProfileView?
    private let keeper = diAssembler.resolve(UserKeeper.self)
    
    override func viewDidLoad() {
        
        navigationController?.navigationBar.prefersLargeTitles = true
        setProfileView()
        super.viewDidLoad()
        
    }
    
    @IBAction func editingSwitch(_ sender: UIBarButtonItem) {
        
        profileView?.inEditingMode = editingRecords
        
        if editingRecords {
            editingRecords = false
            sender.image = #imageLiteral(resourceName: "done")
        } else {
            editingRecords = true
            sender.image = #imageLiteral(resourceName: "edit")
        }
        
    }
    
    /// Настраивает представление пользователя
    private func setProfileView() {
        
        guard let user = keeper?.load() else {
            handleAssertionFailure("Не удалось загрузить данные пользователя")
            return
        }
        source = user
        profileView = viewIfLoaded as? ProfileView
        profileView?.profileSource = self
        
    }
    
}
