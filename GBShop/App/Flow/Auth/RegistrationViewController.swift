//
//  RegistrationViewController.swift
//  GBShop
//
//  Created by Евгений Кириллов on 18/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

final class RegistrationViewController: UITableViewController, TrackableMixin {
    
    // MARK: - Свойства
    
    @IBOutlet private weak var nameContainer: SwitchableTextView!
    @IBOutlet private weak var passwordContainer: SwitchableTextView!
    @IBOutlet private weak var emailContainer: SwitchableTextView!
    @IBOutlet private weak var genderContainer: SwitchableGender!
    @IBOutlet private weak var cardContainer: SwitchableTextView!
    @IBOutlet private weak var bioContainer: SwitchableTextView!
    
    private var fields = [UITextField]()
    private let factory = diAssembler.resolve(AuthRequestFactory.self)
    private let highlighter = diAssembler.resolve(EmptyFieldsChecker.self)
    
    // MARK: - Методы
    
    override func viewDidLoad() {
        
        let containers = [nameContainer,
                          passwordContainer,
                          emailContainer,
                          genderContainer,
                          cardContainer,
                          bioContainer]
        containers.forEach { $0?.isSwitchedOn = true }

        fields = containers.compactMap {
            ($0 as? SwitchableTextView)?.inputField
        }

        super.viewDidLoad()
        
    }
    
    @IBAction func registerTap(_ sender: UIButton) {
        
        guard let highlighter = highlighter,
            !highlighter.didHighlightEmptyFields(fields, { $0.backgroundColor = .red }) else {
                return
        }
        
        let signedUpUser = getEnteredProfile()
        factory?.register(user: signedUpUser) { [weak self] _ in
            self?.track(AnalyticEvents.registration(username: signedUpUser.name, success: true))
            DispatchQueue.main.async {
                self?.performSegue(withIdentifier: "RegToMain", sender: self)
            }
        }
        
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        
        dismiss(animated: true)
        
    }
    
    /// Формирует профиль пользователя из введённых данных
    ///
    /// - Returns: Профиль пользователя
    private func getEnteredProfile() -> UserProfile {
        
        return UserProfile(id: Int(arc4random()),
                           name: nameContainer.text!,
                           password: passwordContainer.text!,
                           email: emailContainer.text!,
                           gender: genderContainer.gender,
                           creditCard: cardContainer.text!,
                           bio: bioContainer.text!)
        
    }
    
}
