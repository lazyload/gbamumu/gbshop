//
//  LoginViewController.swift
//  GBShop
//
//  Created by Евгений Кириллов on 16/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Alamofire

final class LoginViewController: UIViewController, TrackableMixin {
    
    // MARK: - Свойства
    
    @IBOutlet private weak var username: SwitchableTextView!
    @IBOutlet private weak var password: SwitchableTextView!
    @IBOutlet private weak var login: UIButton!
    @IBOutlet private weak var register: UIButton!
    
    private var fields = [UITextField]()
    private let factory = diAssembler.resolve(AuthRequestFactory.self)
    private let keeper = diAssembler.resolve(UserKeeper.self)
    private let highlighter = diAssembler.resolve(EmptyFieldsChecker.self)
    
    // MARK: - Основные методы
    
    override func viewDidLoad() {
        
        let containers = [username, password]
        containers.forEach { $0?.isSwitchedOn = true }
        
        fields = containers.compactMap { $0?.inputField }
        
        login.layer.cornerRadius = login.frame.height / 8
        register.layer.cornerRadius = register.frame.height / 8
        
        super.viewDidLoad()
        
    }
    
    @IBAction func loginTap(_ sender: UIButton) {
        
        guard let highlighter = highlighter,
            !highlighter.didHighlightEmptyFields(fields, { $0.backgroundColor = .red }) else {
            return
        }
        
        factory?.login(userName: username.text!, password: password.text!) { [weak self] loginResult in
            self?.keeper?.save(user: loginResult.user)
            self?.track(AnalyticEvents.login(username: loginResult.user.userLogin, success: true))
            DispatchQueue.main.async {
                self?.performSegue(withIdentifier: "LogToMain", sender: self)
            }
        }
        
    }
    
    @IBAction func hideKeyboardTap(_ sender: UITapGestureRecognizer) {
        
        if username.isFirstResponder {
            username.resignFirstResponder()
        }
        
        if password.isFirstResponder {
            password.resignFirstResponder()
        }
        
    }
    
}
