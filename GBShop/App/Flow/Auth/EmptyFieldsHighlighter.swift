//
//  EmptyFieldsHighlighter.swift
//  GBShop
//
//  Created by Евгений Кириллов on 18/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

/// Помощник для работы с пустыми текстовыми полями
protocol EmptyFieldsChecker {
    
    /// Выделяет пустые поля
    ///
    /// - Parameters:
    ///   - fields: Множество полей для проверки
    ///   - animation: Действие для выделения поля
    /// - Returns: Ответ на вопрос, было ли выделено хоть одно поле
    func didHighlightEmptyFields(_ fields: [UITextField],
                                 _ animation: @escaping (UITextField) -> Void) -> Bool
    
}

/// Помощник, подсвечивающий пустые текстовые поля
final class EmptyFieldsHighlighter: EmptyFieldsChecker {
    
    func didHighlightEmptyFields(_ fields: [UITextField],
                                 _ animation: @escaping (UITextField) -> Void) -> Bool {
        
        var emptyFieldsPresent = false
        
        fields.forEach { testedField in
            
            if testedField.text == nil || testedField.text == "" {
                emptyFieldsPresent = true
                animate(field: testedField) { testedField.backgroundColor = .red }
            }
            
        }
        
        return emptyFieldsPresent
        
    }
    
    private func animate(field: UITextField, _ animation: @escaping () -> Void) {
        
        let oldColor = field.backgroundColor
        
        UIView.animate(withDuration: 1,
                       delay: 0,
                       options: [.autoreverse],
                       animations: animation) { _ in
                        field.backgroundColor = oldColor
        }
        
    }
    
}
