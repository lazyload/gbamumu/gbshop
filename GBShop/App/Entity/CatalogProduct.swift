//
//  CatalogProduct.swift
//  GBShop
//
//  Created by Евгений Кириллов on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Foundation

/// Товар из каталога, возвращающегося от сервера
struct CatalogProduct: Decodable {
    
    let idProduct: Int
    let productName: String
    let price: Int

}
