//
//  LoginResult.swift
//  GBShop
//
//  Created by Евгений Кириллов on 29/10/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Foundation

/// Результат выполнения запроса на вход в учётную запись магазина
struct LoginResult: Decodable {
    
    let result: Int
    let user: User
    
}

/// Данные о пользователе магазина, возвращающиеся от сервера
struct User: Decodable {
    
    let idUser: Int
    let userLogin: String
    let userName: String
    let userLastname: String
    
}
