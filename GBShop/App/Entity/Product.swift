//
//  Product.swift
//  GBShop
//
//  Created by Евгений Кириллов on 05/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Foundation

/// Данные о товаре, возвращающиеся с сервера
struct Product: Decodable {
    
    let productName: String
    let productPrice: Int
    let productDescription: String

}
