//
//  UserProfile.swift
//  GBShop
//
//  Created by Евгений Кириллов on 31/10/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

/// Полная информация о пользователе
struct UserProfile {
    
    let id: Int
    let name: String
    let password: String
    let email: String
    let gender: Gender
    let creditCard: String
    let bio: String

}
