//
//  SimpleResult.swift
//  GBShop
//
//  Created by Евгений Кириллов on 30/10/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Foundation

/// Простой результат, возвращается при выполнении многих запросов
struct SimpleResult: Decodable {
    
    let result: Int
    let userMessage: String?
    
}
