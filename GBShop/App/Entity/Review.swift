//
//  Review.swift
//  GBShop
//
//  Created by Евгений Кириллов on 08/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Foundation

/// Данные об отзыве, возвращающиеся от сервера
struct Review: Decodable {
    
    let reviewId: Int
    let reviewText: String

}
