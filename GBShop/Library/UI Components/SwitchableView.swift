//
//  SwitchableView.swift
//  GBShop
//
//  Created by Евгений Кириллов on 08/12/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

/// Базовый класс для отображения переключаемого представления с текстовым полем
class SwitchableView: XIBView {
    
    @IBInspectable var activeStateColor: UIColor? = UIColor(named: "inputColor")
    @IBInspectable var inactiveStateColor: UIColor? = UIColor.white
    
    var isSwitchedOn = false {
        didSet {
            if isSwitchedOn {
                switchOn()
            } else {
                switchOff()
            }
        }
    }
    
    /// Переводит экземпляр в состояние **включено**. Необходимо переопределить в наследнике
    func switchOn() {}
    
    /// Переводит экземпляр в состояние **отключено**. Необходимо переопределить в наследнике
    func switchOff() {}
    
}
