//
//  SwitchableTextView.swift
//  GBShop
//
//  Created by Евгений on 04/12/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

/// Универсальное поле ввода текста для использования по всему приложению
@IBDesignable final class SwitchableTextView: SwitchableView {

    @IBOutlet weak var inputField: UITextField!
    
    @IBInspectable var placeholder: String? {
        get { return inputField.placeholder }
        set { inputField.placeholder = newValue }
    }
    
    @IBInspectable var text: String? {
        get { return inputField.text }
        set { inputField.text = newValue }
    }
    
    @IBInspectable var secureEntry: Bool {
        get { return inputField.isSecureTextEntry }
        set { inputField.isSecureTextEntry = newValue }
    }
    
    override func switchOn() {
        
        UIView.animate(withDuration: 0.5) {
            self.inputField.isUserInteractionEnabled = true
            self.inputField.backgroundColor = self.activeStateColor
        }
        
    }
    
    override func switchOff() {
        
        UIView.animate(withDuration: 0.5) {
            self.inputField.isUserInteractionEnabled = false
            self.inputField.backgroundColor = self.inactiveStateColor
        }
        
    }

}
