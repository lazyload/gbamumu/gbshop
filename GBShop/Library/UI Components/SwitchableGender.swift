//
//  SwitchableGender.swift
//  GBShop
//
//  Created by Евгений Кириллов on 06/12/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

/// Выбор пола и его отображение в текстовом поле
@IBDesignable final class SwitchableGender: SwitchableView {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var display: UITextField!
    @IBOutlet private weak var switcher: UISegmentedControl!
    @IBOutlet private weak var displayTrailing: NSLayoutConstraint!
    @IBOutlet private weak var switcherTrailing: NSLayoutConstraint!
    
    // MARK: - Properties
    
    @IBInspectable var switcherColor: UIColor? = UIColor.orange
    
    /// Выбранный пол
    var gender: Gender {
        return switcher.selectedSegmentIndex == 0 ? .male : .female
    }
    
    // MARK: - Methods
    
    override func awakeFromNib() {
        
        display.isUserInteractionEnabled = false
        switcher.addTarget(self, action: #selector(changeGender), for: .valueChanged)
        super.awakeFromNib()
        
    }
    
    @objc private func changeGender() {
        
        switch switcher.selectedSegmentIndex {
        case 0: display.text = "Мужской пол"
        default: display.text = "Женский пол"
        }
        
    }
    
    override func switchOn() {
        
        UIView.animate(withDuration: 0.5) {
            self.switcherTrailing.constant = 0
            self.display.backgroundColor = self.activeStateColor
            self.layoutIfNeeded()
        }
        
    }
    
    override func switchOff() {
        
        UIView.animate(withDuration: 0.5) {
            self.switcherTrailing.constant = self.switcher.bounds.width + self.displayTrailing.constant
            self.display.backgroundColor = self.inactiveStateColor
            self.layoutIfNeeded()
        }
        
    }
    
}
