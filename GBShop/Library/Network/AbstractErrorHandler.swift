//
//  AbstractErrorHandler.swift
//  GBShop
//
//  Created by Евгений Кириллов on 22/11/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

/// Обработчик ошибок
protocol AbstractErrorHandler {
    
    /// Обрабатывает переданную ошибку
    ///
    /// - Parameter error: Ошибка для обработки
    func handle(_ error: Error)
    
}
