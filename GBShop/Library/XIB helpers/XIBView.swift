//
//  XIBView.swift
//  GBShop
//
//  Created by Евгений Кириллов on 04/12/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

/// Базовый класс для добавления представления из XIB
class XIBView: UIView, LoadXIBableView {
    
    var xibView: UIView!
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        xibSetup()
        
    }
    
}
