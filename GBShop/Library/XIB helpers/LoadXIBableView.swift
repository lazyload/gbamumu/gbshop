//
//  LoadXIBableView.swift
//  GBShop
//
//  Created by Евгений Кириллов on 04/12/2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import UIKit

/// Определяет способ загрузки представления из XIB-файла
protocol LoadXIBableView: class {

    /// Представление для загрузки из XIB-файла
    var xibView: UIView! { get set }
    
}

extension LoadXIBableView {
    
    /// Загрузка представления из XIB
    func loadViewFromNib() -> UIView? {
        
        let metaData = type(of: self)
        let bundle = Bundle(for: metaData)
        let nib = UINib(nibName: String(describing: metaData), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        return view
        
    }
    
}

extension LoadXIBableView where Self: UIView {
    
    /// Добавляет представление из XIB к текущему представлению
    func xibSetup() {
        
        xibView = loadViewFromNib()
        xibView.frame = bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(xibView)
    
    }
    
}
